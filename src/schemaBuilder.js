import { makeExecutableSchema } from 'graphql-tools';
import { printSchema } from 'graphql';
import getSchemaFromData from './introspection/getSchemaFromData';
import resolver from './resolver';

export default function(data, config) {
    const schemaFromData = getSchemaFromData(data);
    const schemaPrinted = printSchema(schemaFromData);
    const resolverFromData = resolver(data);

    if (config) {
        const { callbackSchema, callbackResolvers } = config;
        if (callbackSchema) callbackSchema(schemaFromData, schemaPrinted);
        if (callbackResolvers) callbackResolvers(resolverFromData);
    }

    return makeExecutableSchema({
        typeDefs: schemaPrinted,
        resolvers: resolverFromData,
        logger: { log: e => console.trace(e) }, // eslint-disable-line no-console
    });
}
